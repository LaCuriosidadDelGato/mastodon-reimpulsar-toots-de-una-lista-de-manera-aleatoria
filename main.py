#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Impulsar un toot tomando su id de un fichero de texto

# CONSTANTES
INST_URL = 'https://botsin.space/'
TOKEN = 'aaaaaaaaa'
NOMFICH = 'toots.txt'

# LIBRERIAS
import time
from random import randint

from mastodon import Mastodon


# FUNCIONES
def url_toot():
    f = open(NOMFICH)
    urls = f.readlines()
    f.close()
    numAleatorio = randint(0,len(urls)-1)
    return urls[numAleatorio].strip()

def reimpulsar(url,api):
    estado = api.search_v2(url)
    
    if len(estado.statuses)!=1:
        return 'No existe el estado'

    id = estado.statuses[0].id
        
    try:
        status = api.status_unreblog(id)
        time.sleep(2)
    except:
        pass
    
    try:
        api.status_reblog(id)
        return 'Impulso hecho'
    except:
        return 'No se puede impulsar'
        
# MAIN
masto = Mastodon(
    access_token = TOKEN,
    api_base_url = INST_URL
)

mensaje = reimpulsar(url_toot(),masto)       
print(mensaje)
